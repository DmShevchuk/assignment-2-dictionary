ASM_FLAGS=-felf64
DIR=./

main: main.o lib.o dict.o
	ld -o $@ $^

%.o: %.asm
	nasm $(ASM_FLAGS) $< -o $@

.PHONY: clean

clean:
	rm $(DIR)*.o
