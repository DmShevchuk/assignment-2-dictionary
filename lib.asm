%define EXIT 60
%define READ 0
%define WRITE 1
%define SIN 0
%define SOUT 1
%define END_LINE 0x0
%define NEXT_LINE 0xA
%define ASCII_WHITESPACE 0x20
%define ASCII_TAB 0x9
%define SERROR 2

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax
        .loop:
                cmp byte [rdi + rax], END_LINE
                je .end
                inc rax
                jmp .loop
        .end:
                ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
        call string_length
	pop rdi
        mov rdx, rax 
        mov rax, WRITE
        mov rsi, rdi
        mov rdi, SOUT  
        syscall
        ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi                                                    
	mov rsi, rsp
	mov rdx, 1
	mov rax, WRITE
	mov rdi, SOUT
	syscall
	pop rdi
	ret        

; Переводит строку (выводит символ с кодом 0xA)
print_newline:                                                             
        mov rdi, NEXT_LINE                                                
        jmp print_char                                                                                           

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor rax, rax
	xor rcx, rcx
	dec rsp
	mov [rsp], al 
	mov rax, rdi
	mov r8, 10
	
	.loop:
		inc rcx
		xor rdx, rdx
		div r8
		add rdx, '0'
		dec rsp
		mov [rsp], dl
		test rax, rax
		jnz .loop
		mov rdi, rsp
		push rcx
		call print_string
		pop rcx
		add rsp, rcx
		inc rsp
	ret 

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
        cmp rdi, 0
	jl .lz
	jmp .end	

	.lz:
		push rdi
		mov rdi, '-'
		call print_char
		pop rdi
		neg rdi
	.end:	
		jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor r9, r9
	xor r10, r10
	xor rcx, rcx
	.loop:
		mov byte r9b, [rdi + rcx]
		mov byte r10b, [rsi + rcx]
                cmp r9b, r10b
                jne .not_eq
                test r9b, r9b
                jz .eq
                inc rcx
                jmp .loop
	.not_eq:
		mov rax, 0
		jmp .end
	.eq:
		mov rax, 1
	.end:
		xor rcx, rcx
		ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	push rax
	mov rdi, SIN
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rax, rax
        xor rcx, rcx
	.skip:
		push rdi
		push rcx
		push rsi
		call read_char
		pop rsi
		pop rcx
		pop rdi
		cmp rax, 0
		je .ok
		cmp rcx, rsi
		je .fail
		cmp rax, ASCII_WHITESPACE
		je .skip
		cmp rax, ASCII_TAB
		je .skip
		cmp rax, NEXT_LINE
		je .skip
	
	.loop:
		cmp rax, 0
		je .ok		
		cmp rcx, rsi
		je .fail
		cmp rax, ASCII_WHITESPACE
		je .ok
		cmp rax, ASCII_TAB
		je .ok
		cmp rax, NEXT_LINE
		je .ok
		mov [rdi + rcx], rax
		inc rcx
		push rdi
		push rcx
		push rsi
		call read_char
		pop rsi
		pop rcx
		pop rdi
		jmp .loop
	.ok:
		mov byte [rdi + rcx], END_LINE
		mov rax, rdi
		mov rdx, rcx
		jmp .end
	.fail:
		xor rax, rax
	.end:
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	xor r9, r9	
	mov r8, 10

	.loop:
		mov byte r9b, [rdi + rdx]
		cmp r9b, '0'
		jl .end
		cmp r9b, '9'
		jg .end
		push rdx
		mul r8
		pop rdx
		sub r9, '0'
		add al, r9b
		inc rdx
		jmp .loop
	.end:
		ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rsi, rsi
	cmp byte [rdi], '-'
	jne .loop
	mov rsi, 1
	inc rdi
	
	.loop:
		push rsi
		call parse_uint
		pop rsi
		cmp rdx, 0
		je .end
		cmp rsi, 0
		je .end
		inc rdx
		neg rax
	.end:
		ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rcx, rcx
	
	push rdi
	push rsi
	push rdx
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rdx, rax
	jbe .error
	
	.loop:
		cmp rdx, 0
		je .end
		xor r10, r10
		mov byte r10b, [rdi + rcx]
		mov byte [rsi + rcx], r10b
		inc rcx
		dec rdx
		cmp r10, 0
		jne .loop

	.error:
		xor rax, rax

	.end:
		ret

print_error:
	push rdi
        call string_length
	pop rdi
        mov rdx, rax
        mov rax, WRITE
        mov rsi, rdi
        mov rdi, SERROR
        syscall
        ret
