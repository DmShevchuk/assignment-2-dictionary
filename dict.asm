%include "lib.inc"

global find_word

section .text

find_word:
	.loop:
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi

		cmp rax, 0
		jne .exists

		mov rsi, [rsi]
		test rsi, rsi
		jnz .loop

		jmp .not_exists

	.exists:
		mov rax, rsi
		ret

	.not_exists:
		xor rax, rax
		ret
