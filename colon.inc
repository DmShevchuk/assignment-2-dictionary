%define NEXT_ELEMENT 0

%macro colon 2

        %ifid %2
                %2: dq NEXT_ELEMENT
        %else
                %error "Неверное значение id!"
        %endif


        %ifstr %1
                db %1, 0
        %else
                %error "Ключ должен быть строкой!"
        %endif


	%define NEXT_ELEMENT %2

%endmacro

