%include "colon.inc"
%include "lib.inc"
%include "word.inc"

extern find_word

%define BUFFER_CAPACITY 256
%define POINTER_SIZE 8

global _start

section .rodata
        INPUT_LINE: db "Введите ключ:", 0
	VALUE_NOT_FOUND_MESSAGE: db "Значение по ключу не найдено!", 0
        BUFFER_OVERFLOW_MESSAGE: db "Переполнение буфера!", 0
	DELIMITER: db "------------>", 0


section .bss
        BUFFER: resb BUFFER_CAPACITY


section .text

_start:
	xor rax, rax
	xor rdi, rdi
        mov rdi, INPUT_LINE
        call print_string
        mov rdi, BUFFER
        mov rsi, BUFFER_CAPACITY
        call read_word
        test rax, rax
        jz .buffer_overflow

.search_value:
	push rdx
        mov rdi, rax
        mov rsi, NEXT_ELEMENT
        call find_word
        test rax, rax
        jz .value_not_found
	jmp .print_value

.buffer_overflow:
        mov rdi, BUFFER_OVERFLOW_MESSAGE
        jmp .fail

.value_not_found:
        mov rdi, VALUE_NOT_FOUND_MESSAGE
	jmp .fail

.print_value:
	add rax, POINTER_SIZE
	mov rdi, rax
	push rdi
	mov rdi, DELIMITER
	call print_string
	pop rdi
	pop rdx
	add rdi, rdx
	inc rdi
        call print_string
	call print_newline
	xor rdi, rdi
        call exit

.fail:
	call print_error
	mov rdi, 1
	call exit
